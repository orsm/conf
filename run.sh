#!/bin/bash

domain=www.example.com
auth=www.example.com
psk=www.example.com

snap install --classic certbot

/snap/bin/certbot certonly --standalone -d $domain

apt update

apt install -y libnss-ldapd ldap-utils libpam-ldapd rsyslog-mysql nslcd

#apt install -y apache2 libapache2-mod-authnz-pam php

apt install -y vim squid ocserv dante-server openssl ca-certificates

apt install -y freeradius-utils libpam-radius-auth

apt install -y strongswan strongswan-pki libcharon-extra-plugins libcharon-extauth-plugins libstrongswan-extra-plugins

#exit 0

bash -c 'mv /etc/squid/squid.conf{,.bak}'
bash -c 'mv /etc/ocserv/ocserv.conf{,.bak}'
bash -c 'mv /etc/ipsec.conf{,.bak}'
bash -c 'mv /etc/danted.conf{,.bak}'
bash -c 'mv /etc/nsswitch.conf{,.bak}'
bash -c 'mv /etc/pam.d/common-auth{,.bak}'
bash -c 'mv /etc/rsyslog.d/mysql.conf{,.bak}'

cp ./files/squid.conf /etc/squid/squid.conf
cp ./files/ocserv.conf /etc/ocserv/ocserv.conf
cp ./files/ipsec.conf /etc/ipsec.conf
cp ./files/danted.conf /etc/danted.conf
cp ./files/nsswitch.conf /etc/nsswitch.conf

cp ./files/pam.bak /etc/pam.d/ipsec
cp ./files/pam.bak /etc/pam.d/ocserv
cp ./files/pam.bak /etc/pam.d/squid
cp ./files/pam.bak /etc/pam.d/danted

sed -i "s/domain.name/$domain/g" /etc/squid/squid.conf
sed -i "s/domain.name/$domain/g" /etc/ocserv/ocserv.conf

#sed -i "s/Listen 443/#Listen 443/g" /etc/apache2/ports.conf

sed -i "s/$ActionFileDefaultTemplate RSYSLOG_TraditionalFileFormat/#$ActionFileDefaultTemplate RSYSLOG_TraditionalFileFormat/g" /etc/rsyslog.conf

sed '1i\auth sufficient pam_radius_auth.so' /etc/pam.d/common-auth.bak >> /etc/pam.d/common-auth

bash -c 'echo -e "\n$auth secret 5" >> /etc/pam_radius_auth.conf'

bash -c 'echo -e "\nnet.ipv4.ip_forward=1" >> /etc/sysctl.conf && sysctl -p'

bash -c 'echo -e "\n : PSK '$psk'" >> /etc/ipsec.secrets'

bash -c 'echo -e "\n" >> /etc/ufw/before.rules' && bash -c 'cat ./files/iptables.bak >> /etc/ufw/before.rules'

sed -i "s/auth.domain.name/$auth/g" ./files/mysql.rsyslog.conf

bash -c 'cat ./files/mysql.rsyslog.conf >> /etc/rsyslog.d/mysql.conf'

#exit 0

date -R

tzselect << EOF
4
10
1
EOF

cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

hwclock --systohc

useradd -s /bin/false socks

chmod 644 /etc/pam_radius_auth.conf
chmod +s /etc/pam_radius_auth.conf

ufw allow 22/tcp
ufw allow 80/tcp
ufw allow 443/tcp
ufw allow 21/tcp
ufw allow 23/tcp
ufw allow 4500/udp
ufw allow 500/udp

ufw enable << EOF
y
EOF

#reboot
